package com.mfs.client.touchpay.dto;

public class InitiateTransactionRequestDto {
	private String idFromClient;
	private AdditionnalInfos additionnalInfos;
	private String amount;
	private String callback;
	private String recipientNumber;
	private String serviceCode;
	public String getIdFromClient() {
		return idFromClient;
	}
	public void setIdFromClient(String idFromClient) {
		this.idFromClient = idFromClient;
	}
	public AdditionnalInfos getAdditionnalInfos() {
		return additionnalInfos;
	}
	public void setAdditionnalInfos(AdditionnalInfos additionnalInfos) {
		this.additionnalInfos = additionnalInfos;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCallback() {
		return callback;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	public String getRecipientNumber() {
		return recipientNumber;
	}
	public void setRecipientNumber(String recipientNumber) {
		this.recipientNumber = recipientNumber;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	@Override
	public String toString() {
		return "InitiateTransactionRequestDto [idFromClient=" + idFromClient + ", additionnalInfos=" + additionnalInfos
				+ ", amount=" + amount + ", callback=" + callback + ", recipientNumber=" + recipientNumber
				+ ", serviceCode=" + serviceCode + "]";
	}
	
	
	
}
