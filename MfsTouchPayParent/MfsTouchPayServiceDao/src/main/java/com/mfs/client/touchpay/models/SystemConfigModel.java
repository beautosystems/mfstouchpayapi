package com.mfs.client.touchpay.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "touchpay_system_config")
public class SystemConfigModel {
	
	@Id
	@GeneratedValue
	@Column(name = "system_config_id")
	int configId;

	@Column(name = "config_key")
	String configKey;

	@Column(name = "config_value")
	String configValue;

	public int getConfigId() {
		return configId;
	}

	public void setConfigId(int configId) {
		this.configId = configId;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	@Override
	public String toString() {
		return "SystemConfigModel [configId=" + configId + ", configKey=" + configKey + ", configValue=" + configValue
				+ "]";
	}
	
}
