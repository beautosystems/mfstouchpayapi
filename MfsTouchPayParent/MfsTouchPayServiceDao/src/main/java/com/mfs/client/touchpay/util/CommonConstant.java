package com.mfs.client.touchpay.util;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String AUTH_URL="auth_url";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String AUTO_DETECT_MOBILE_URL = "auto_detect_mobile_url";
	

	public static final String PORT = "port";
	public static final String ISHTTPS = "ishttps";
	
	public static final String AUTHORIZATION = "Authorization";
	public static final String ACCEPT = "accept";
	public static final String BEARER = "Bearer ";
	
	public static final String CLIENT_ID="client_id";
	public static final String CLIENT_SECRET="client_secret";
	public static final String GRANT_TYPE="grant_type";
	public static final String AUDIENCE="audience";
	
	public static final String CACHE_CONTROL = "Cache-Control";
	public static final String NO_CACHE= "no-cache";
	public static final String HOST = "Host";
	public static final String TOPUPS_RELOADLY_COM = "topups.reloadly.com";
	public static final String ACCOUNT_BALANCE = "account_balance";
	public static final String TOPUPS_REPORTS_TRANSACTIONS = "topups_reports_transactions";
	
	public static final String OPERATORS = "operators";
	public static final String TOPUPS = "topups";
	public static final String COUNTRIES = "countries";
	public static final String PROMOTIONS = "promotions";
	
	public static final String INVALID_RECIPIENT_COUNTRY_CODE = "Invalid Recipient Country Code";
	public static final String INVALID_SENDER_COUNTRY_CODE = "Invalid Sender Country Code";
	public static final String INVALID_RECIPIENT_PHONE = "Invalid reciepient phone";
	public static final String INVALID_SENDER_PHONE = "invalid sender phone";
	public static final String INVALID_OPERATOR_ID = "Invalid Operator Id";
	public static final String INVALID_AMOUNT = "Invalid Amount";
	public static final String INVALID_CUSTOMER_IDENTIFIER = "Invalid Customer Identifier";
	public static final String INVALID_MFS_TRANSID = "Invalid Mfs Trans ID";
	public static final String DELAYTIME = "delayTime";
	public static int expirationTime=2;
	
	public static final int MINUETS = 86400;
	public static final int SECONDS = 5184000;
	public static final int ONE_DAY = 60;
	
	public static final String LIST_ALL_COMMISSION_URL = "list_all_commission";
	public static final String COMMISSIONS = "commissions";

}
