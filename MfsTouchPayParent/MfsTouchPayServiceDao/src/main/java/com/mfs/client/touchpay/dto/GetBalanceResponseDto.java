package com.mfs.client.touchpay.dto;

public class GetBalanceResponseDto {
	private String status;
	private String amount;
	private String errorCode;
	private String errorMessage;
	private String code;
	private String messages;
	
	
	
	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getErrorCode() {
		return errorCode;
	}



	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}



	public String getErrorMessage() {
		return errorMessage;
	}



	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getMessages() {
		return messages;
	}



	public void setMessages(String messages) {
		this.messages = messages;
	}



	@Override
	public String toString() {
		return "GetBalanceResponseDto [status=" + status + ", amount=" + amount + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", code=" + code + ", messages=" + messages + "]";
	}
	
	

}
