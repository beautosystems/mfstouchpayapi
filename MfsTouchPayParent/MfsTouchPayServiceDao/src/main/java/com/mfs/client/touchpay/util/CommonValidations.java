package com.mfs.client.touchpay.util;

/**
 * @ author Mohsin Shaikh
 *  CommonValidations.java 
 *  27-Jan-2020
 * 
 * purpose : this class is used for common Validation
 * 
 * Methods 
 * 1.validateStringValues
 * 2.validateIntValues
 * 3.validateAmountValues
 * 4.validatePhoneNumber
 * 
 * Change_history
 */
import java.util.regex.Pattern;

public class CommonValidations {
	
	// Validation to check String Values Null or empty
	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		if (reqParamValue == null || reqParamValue.equals(""))
			validateResult = true;

		return validateResult;
	}

	// check for Integer Value null or not
	public boolean validateIntValues(Integer reqParamValue) {
		boolean validateResult = false;
		if (reqParamValue == null || reqParamValue.equals("") || reqParamValue == 0)
			validateResult = true;

		return validateResult;
	}

	// Validation to check Amount Values null
	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null  || amount==0)
			validateResult = true;

		return validateResult;
	}

	// Validation to check phone number with + and without + sign
		public boolean validatePhoneNumber1(String number) {
			boolean result = true;

			Pattern pattern = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");
		    Pattern pattern1 = Pattern.compile("([+]?[0-9][0-9]*){6,14}[0-9]");

			if (pattern.matcher(number).matches() || pattern1.matcher(number).matches()) {
				result = false;
			}
			else if(number == null || number.equals("")) {
				result = true;
			}
			return result;
		}

}
