package com.mfs.client.touchpay.dto;

public class AdditionnalInfos {
	private String recipientEmail;
	private String recipientFirstName;
	private String recipientLastName;
	private String destinataire;
	
	public String getRecipientEmail() {
		return recipientEmail;
	}
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}
	public String getRecipientFirstName() {
		return recipientFirstName;
	}
	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}
	public String getRecipientLastName() {
		return recipientLastName;
	}
	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}
	@Override
	public String toString() {
		return "AdditionnalInfos [recipientEmail=" + recipientEmail + ", recipientFirstName=" + recipientFirstName
				+ ", recipientLastName=" + recipientLastName + ", destinataire=" + destinataire + "]";
	}
	
}
