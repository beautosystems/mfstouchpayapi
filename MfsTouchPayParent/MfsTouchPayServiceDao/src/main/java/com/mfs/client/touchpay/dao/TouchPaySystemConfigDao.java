package com.mfs.client.touchpay.dao;

import java.util.Map;

public interface TouchPaySystemConfigDao {

	public Map<String, String> getConfigDetailsMap();

}
