package com.mfs.client.touchpay.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "touchpay_transaction_log")
public class TouchPayTransactionLogModel {
	
	@Id
	@GeneratedValue
	@Column(name = "transaction_log_id")
	private int transactionId;
	
	@Column(name = "id_from_client")
	private String idFromClient;
	
	@Column(name = "recipient_email")
	private String recipientEmail;
	
	@Column(name = "recipient_first_name")
	private String recipientFirstName;
	
	@Column(name = "recipient_last_name")
	private String recipientLastName;
	
	@Column(name = "destinataire")
	private String destinataire;
	
	@Column(name = "amount")
	private String amount;
	
	@Column(name = "callback")
	private String callback;
	
	@Column(name = "recipient_number")
	private String recipientNumber;
	
	@Column(name = "service_code")
	private String serviceCode;
	
	@Column(name = "id_from_gu")
	private String idFromGu;
	
	@Column(name = "fees")
	private Double fees;
	
	@Column(name = "date_time")
	private String dateTime;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "date_logged")
	private Date dateLogged;

}
