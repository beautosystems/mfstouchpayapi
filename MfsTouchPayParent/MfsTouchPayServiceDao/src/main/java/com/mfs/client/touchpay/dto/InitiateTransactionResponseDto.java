package com.mfs.client.touchpay.dto;

public class InitiateTransactionResponseDto 
{
	private String idFromClient;
	private String idFromGU;
	private String amount;
	private String serviceCode;
	private String recipintNumber;
	private String dateTime;
	private String status;
	private String code;
	private String message;
	public String getIdFromClient() {
		return idFromClient;
	}
	public void setIdFromClient(String idFromClient) {
		this.idFromClient = idFromClient;
	}
	public String getIdFromGU() {
		return idFromGU;
	}
	public void setIdFromGU(String idFromGU) {
		this.idFromGU = idFromGU;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getRecipintNumber() {
		return recipintNumber;
	}
	public void setRecipintNumber(String recipintNumber) {
		this.recipintNumber = recipintNumber;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "InitiateTransactionResponseDto [idFromClient=" + idFromClient + ", idFromGU=" + idFromGU + ", amount="
				+ amount + ", serviceCode=" + serviceCode + ", recipintNumber=" + recipintNumber + ", dateTime="
				+ dateTime + ", status=" + status + ", code=" + code + ", message=" + message + "]";
	}
	
	
}
