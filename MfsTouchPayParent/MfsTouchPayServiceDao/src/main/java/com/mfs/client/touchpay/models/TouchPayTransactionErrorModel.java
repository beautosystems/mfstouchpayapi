package com.mfs.client.touchpay.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "touchpay_transaction_error_log")
public class TouchPayTransactionErrorModel {

	@Id
	@GeneratedValue
	@Column(name = "transaction_error_id")
	private int transactionErrorId;
	
	@Column(name = "id_from_client")
	private String idFromClient;
	
	@Column(name = "recipient_email")
	private String recipientEmail;
	
	@Column(name = "recipient_first_name")
	private String recipientFirstName;
	
	@Column(name = "recipient_last_name")
	private String recipientLastName;
	
	@Column(name = "destinataire")
	private String destinataire;
	
	@Column(name = "amount")
	private String amount;
	
	@Column(name = "callback")
	private String callback;
	
	@Column(name = "recipient_number")
	private String recipientNumber;
	
	@Column(name = "service_code")
	private String serviceCode;
	
	@Column(name = "error_code")
	private String errorCode;
	
	@Column(name = "error_message")
	private Double errorMessage;
	
	@Column(name = "date_logged")
	private Date dateLogged;

	
}
