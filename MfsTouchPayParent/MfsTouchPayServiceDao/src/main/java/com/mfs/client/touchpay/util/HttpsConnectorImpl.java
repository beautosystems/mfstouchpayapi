package com.mfs.client.touchpay.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class HttpsConnectorImpl {

	private static final int CHECK_RESPONSE_CODE201 = 201;
	private static final int CHECK_RESPONSE_CODE200 = 200;
	private static final int CHECK_RESPONSE_CODE202 = 202;

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException, Exception {
		SSLContext sslContext = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		String responseData = null;

		try {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			throw new Exception(e);
		}

		//

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());
		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());
		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		httpsConnection.setDoInput(true);
		httpsConnection.setDoOutput(true);
		httpsConnection.setReadTimeout(500000);

		if (connectionData != null) {
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}

		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == ReloadlyTopupCodes.S201.getCode() || responseCode == ReloadlyTopupCodes.S200.getCode()
				|| responseCode == ReloadlyTopupCodes.S202.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == ReloadlyTopupCodes.ER400.getCode()) {

			connectionResponse.setCode(ResponseCodes.INVALID_REQUEST.getCode());
			connectionResponse.setTxMessage(ResponseCodes.INVALID_REQUEST.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER401.getCode()) {
			connectionResponse.setCode(ResponseCodes.AUTHENTICATION_FAILURE.getCode());
			connectionResponse.setTxMessage(ResponseCodes.AUTHENTICATION_FAILURE.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER403.getCode()) {
			connectionResponse.setCode(ResponseCodes.NOT_AUTHORIZED.getCode());
			connectionResponse.setTxMessage(ResponseCodes.NOT_AUTHORIZED.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER404.getCode()) {
			connectionResponse.setCode(ResponseCodes.RESOURCE_NOT_FOUND.getCode());
			connectionResponse.setTxMessage(ResponseCodes.RESOURCE_NOT_FOUND.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER405.getCode()) {
			connectionResponse.setCode(ResponseCodes.METHOD_NOT_SUPPORTED.getCode());
			connectionResponse.setTxMessage(ResponseCodes.METHOD_NOT_SUPPORTED.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER406.getCode()) {
			connectionResponse.setCode(ResponseCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getCode());
			connectionResponse.setTxMessage(ResponseCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER415.getCode()) {
			connectionResponse.setCode(ResponseCodes.UNSUPPORTED_MEDIA_TYPE.getCode());
			connectionResponse.setTxMessage(ResponseCodes.UNSUPPORTED_MEDIA_TYPE.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER422.getCode()) {
			connectionResponse.setCode(ResponseCodes.UNPROCCESSABLE_ENTITY.getCode());
			connectionResponse.setTxMessage(ResponseCodes.UNPROCCESSABLE_ENTITY.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER429.getCode()) {
			connectionResponse.setCode(ResponseCodes.RATE_LIMIT_REACHED.getCode());
			connectionResponse.setTxMessage(ResponseCodes.RATE_LIMIT_REACHED.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER500.getCode()) {
			connectionResponse.setCode(ResponseCodes.INTERNAL_SERVER_ERROR.getCode());
			connectionResponse.setTxMessage(ResponseCodes.INTERNAL_SERVER_ERROR.getMessage());

		} else if (responseCode == ReloadlyTopupCodes.ER503.getCode()) {

			connectionResponse.setCode(ResponseCodes.SERVICE_UNAVAILABLE.getCode());
			connectionResponse.setTxMessage(ResponseCodes.SERVICE_UNAVAILABLE.getMessage());
		} else {

			connectionResponse.setCode(ResponseCodes.E401.getCode());
			connectionResponse.setTxMessage(ResponseCodes.E401.getMessage());

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		if (responseCode == ReloadlyTopupCodes.S200.getCode() && output.equals("")) {
			connectionResponse.setTxMessage(ResponseCodes.S200.getMessage());
			connectionResponse.setRespCode(responseCode);
		} else if (output != null) {
			connectionResponse.setResponseData(output);
			connectionResponse.setRespCode(responseCode);
			connectionResponse.setTxMessage(ResponseCodes.S200.getMessage());
		}

		return connectionResponse;
	}

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */
	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		SSLContext sslContext = null;

		try {
			sslContext = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
		}
		try {
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		// Send post request
		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE200 || responseCode == CHECK_RESPONSE_CODE201
				|| responseCode == CHECK_RESPONSE_CODE202) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		String output = readConnectionDataWithBuffer(httpsResponse);

		if (output == null || output.isEmpty()) {
			if (responseCode == CHECK_RESPONSE_CODE201 || responseCode == CHECK_RESPONSE_CODE200
					|| responseCode == CHECK_RESPONSE_CODE202) {
				output = "OK";
			}
		}

		return httpsResponse;
	}

	public static void wait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * @param httpsResponse
	 * @return
	 * @throws IOException
	 */
	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	/**
	 * Set Header Properties To HTTPS Connection
	 * 
	 * @param httpsConnectionRequest
	 * @param connectionData
	 * @param httpsConnection
	 */
	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		}

	}

	/**
	 * Set headers that are required by calling app
	 * 
	 * @param headers
	 * @param httpsConnection
	 */
	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}

	}

}