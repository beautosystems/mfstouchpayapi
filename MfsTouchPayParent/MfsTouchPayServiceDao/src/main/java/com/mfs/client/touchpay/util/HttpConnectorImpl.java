package com.mfs.client.touchpay.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnectorImpl {

	public HttpsConnectionResponse httpUrlConnection(HttpsConnectionRequest httpRequest, String request)
			throws Exception {
		HttpURLConnection httpCon = null;
		URL url = null;
		StringBuffer response = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		try {
			url = new URL(httpRequest.getServiceUrl());

			httpCon = (HttpURLConnection) url.openConnection();

			httpCon.setRequestMethod(httpRequest.getHttpmethodName());

			httpCon.setDoOutput(true);

			httpCon.setRequestProperty("charset", "utf-8");

			httpCon.setRequestProperty("Content-type", "application/json");

			httpCon.getOutputStream().write(request.getBytes());

			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));

			String inputLine = null;

			response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			if (response != null) {
				connectionResponse.setResponseData(response.toString());
			}

		} catch (Exception e) {
			throw new Exception(e);
		}

		return connectionResponse;
	}
}
